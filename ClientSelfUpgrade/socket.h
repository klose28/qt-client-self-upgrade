#ifndef SOCKET_H
#define SOCKET_H

#include <QTimer>
#include <QObject>
#include <QSettings>
#include <QTcpSocket>

class Socket : public QObject
{
    Q_OBJECT
public:
    explicit Socket(QObject *parent = nullptr);
public slots:
    void onSocketInit();
signals:
    void signalSendToThread(QByteArray data);
    void signalSendTcpState(QAbstractSocket::SocketState socketState);
public slots:
    /*重连*/
    void timerOut();
    /*tcp处理*/
    void onStateChanged(QAbstractSocket::SocketState socketState);
    void onSocketError(QAbstractSocket::SocketError lse);
    void onReadyRead();
    void onSendToServer(QByteArray data);
private:
    QTcpSocket* socket;
    QTimer* t;
    QSettings* settingsVersions;
    QByteArray sourceData;
};

#endif // SOCKET_H
