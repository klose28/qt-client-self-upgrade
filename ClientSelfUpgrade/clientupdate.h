#ifndef CLIENTUPDATE_H
#define CLIENTUPDATE_H

#include <QWidget>
#include <QTcpSocket>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>
#include <QFileInfo>
#include <QThread>
#include <QCryptographicHash>
#include <QMessageBox>
#include <QTimer>
#include <QDataStream>
#include <QDir>
#include <QProcess>

#include "socket.h"

enum Type {
    INVALID = -1,
    UPDATE, //更新确认
    START, //文件开始
    DATA, //传输中
    FINISH, //文件完成
    ERROR,  //错误
    VERSIONS_EXAMINE //版本检查
};

struct FileData
{
    QString fileName;
    QString MD5;
    QByteArray fileContent;
    FileData()
        : fileName("")
        , MD5("")
        , fileContent("")
    {}
};

struct TcpData
{
    Type type;
    QByteArray data;
    TcpData()
        : data("")
    {}
};

namespace Ui {
class ClientUpdate;
}

class ClientUpdate : public QWidget
{
    Q_OBJECT

public:
    explicit ClientUpdate(QWidget *parent = nullptr);
    ~ClientUpdate();

private slots:
    /*接受tcp数据*/
    void onReadyRead(QByteArray data);
    void onStateChanged(QAbstractSocket::SocketState socketState);
    /*初始化*/
    void onCuInit();
    
    void onPushButton();
private:
    template <typename T>
    QByteArray convertToByte(T v);
    /*结构体转换*/
    QByteArray convert(FileData fd);
    FileData convert(QByteArray data);
    /*传输封包*/
    QByteArray envelop(TcpData tData);
    TcpData unpack(QByteArray& data);
    /*获取绝对路径MD5*/
    static const QString countMd5(const QString filePath);
    /*接受状态处理*/
    void typeUpdate(FileData fData);
    void typeData(FileData fData);
    void typeStart(FileData fData);
    void typeFinish(FileData fData);

    void qMessageBox();
signals:
    void signalSendToServer(QByteArray data);
    void signalSocketInit();
private:
    void datProcessing(TcpData tData);
private:
    Ui::ClientUpdate *ui;
    QSettings* settingsVersions;
    int fileSum; //保存文件数量
    double value; //保存进度条每次增加大小
    QByteArray sourceData;
    QString updateVersions;
    QString currentVersions;
};

#endif // CLIENTUPDATE_H
